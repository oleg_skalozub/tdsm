package segment

import (
	"fmt"
	"time"
)

func TimeStampConvertor(miliSec, date []byte) (time.Time, error) {
	loc, _ := time.LoadLocation("")
	seconds, err := GetInt64(date)
	if err != nil {
		return time.Time{}, err
	}
	halfSeconds, err := GetUint32(miliSec)
	if err != nil {
		return time.Time{}, err
	}

	fmt.Println(halfSeconds)

	startDate := time.Date(1904, 1, 1, 0, 0, 0, 0, loc)
	resDate := startDate.Add(time.Second * time.Duration(seconds))
	return resDate, err
}
