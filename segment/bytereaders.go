package segment

import (
	"bytes"
	"encoding/binary"
)

func GetUint32(file []byte) (uint32, error) {
	var value uint32
	buf := bytes.NewReader(file)
	err := binary.Read(buf, binary.LittleEndian, &value)
	if err != nil {
		return 0, err
	}

	return value, nil
}

func GetUint64(file []byte) (uint64, error) {
	var value uint64
	buf := bytes.NewReader(file)
	err := binary.Read(buf, binary.LittleEndian, &value)
	if err != nil {
		return 0, err
	}

	return value, nil
}

func GetInt64(file []byte) (int64, error) {
	var value int64
	buf := bytes.NewReader(file)
	err := binary.Read(buf, binary.LittleEndian, &value)
	if err != nil {
		return 0, err
	}

	return value, nil
}

func GetBytes(file []byte, size int, current *int) []byte {
	res := file[*current : *current+size]
	*current = *current + size
	return res
}

func GetFloat64(file []byte) (float64, error) {
	var value float64
	buf := bytes.NewReader(file)
	err := binary.Read(buf, binary.LittleEndian, &value)
	if err != nil {
		return 0, err
	}

	return value, nil
}
