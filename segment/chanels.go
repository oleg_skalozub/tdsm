package segment

import (
	"time"
)

type Chanel struct {
	NextObjectLen          uint32
	PropertyName           string
	LengthIndexInformation uint32
	DataType               uint32
	Dimension              uint32
	RowDataValues          uint64
	NumberProperties       uint32
	LenWfStartTime         uint32
	NameOfWfStartTime      string
	DataTypeWfStartTime    uint32
	TimeStampWfStartTime   time.Time
	LenOfWfStartOffs       uint32
	NameOfWfStartOffs      string
	DataTypeWfStartOffs    uint32
	ValueWfStartOffs       float64
	LenOfWfIncrement       uint32
	NameOfWfIncrement      string
	DataTypeWfIncrement    uint32
	ValueOfWfIncrement     float64
	LenOfWfSamples         uint32
	NameOfWfSamples        string
	DataTypeWfSamples      uint32
	ValueTypeWfSamples     uint32
	ChanelLenght           uint64
	RowData                []float64
}

func NewChannel(file []byte, position *int) (Chanel, error) {
	var channel Chanel
	var err error

	channel.NextObjectLen, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.PropertyName = string(GetBytes(file, int(channel.NextObjectLen), position))
	channel.LengthIndexInformation, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.DataType, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.Dimension, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.RowDataValues, err = GetUint64(GetBytes(file, 8, position))
	if err != nil {
		return channel, err
	}

	channel.NumberProperties, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.LenWfStartTime, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.NameOfWfStartTime = string(GetBytes(file, int(channel.LenWfStartTime), position))
	channel.DataTypeWfStartTime, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.TimeStampWfStartTime, err = TimeStampConvertor(GetBytes(file, 4*2, position), GetBytes(file, 4*2, position))
	if err != nil {
		return channel, err
	}

	channel.LenOfWfStartOffs, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.NameOfWfStartOffs = string(GetBytes(file, int(channel.LenOfWfStartOffs), position))
	channel.DataTypeWfStartOffs, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.ValueWfStartOffs, err = GetFloat64(GetBytes(file, 8, position))
	if err != nil {
		return channel, err
	}

	channel.LenOfWfIncrement, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.NameOfWfIncrement = string(GetBytes(file, int(channel.LenOfWfIncrement), position))
	channel.DataTypeWfIncrement, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.ValueOfWfIncrement, err = GetFloat64(GetBytes(file, 8, position))
	if err != nil {
		return channel, err
	}

	channel.LenOfWfSamples, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.NameOfWfSamples = string(GetBytes(file, int(channel.LenOfWfSamples), position))
	channel.DataTypeWfSamples, err = GetUint32(GetBytes(file, 4, position))
	if err != nil {
		return channel, err
	}

	channel.ValueTypeWfSamples, err = GetUint32(GetBytes(file, 4, position))

	return channel, nil
}
