package segment

import (
	"fmt"
	"strconv"
)

//@see http://www.ni.com/product-documentation/5696/en/
const (
	NumberOfObjects         = 4
	LengthFirstObjectPath   = 4
	RawDataIndex            = 4
	NumberOfProperties      = 4
	LengthFirstPropertyName = 4
	DataTypePropertyValue   = 4
	LengthPropertyValue     = 4
)

const (
	tdsTypeU16 = 7
	//TODO add 7 and 28
)

type MetaData struct {
	Position, Size        int
	NewObjects            uint32
	LengthFirstObjectPath uint32
	ObjectPath            string
	RawDataIndex          []byte
	NumberOfProperties    uint32
	Properties            []Property
	Channels              []Chanel
}

type Property struct {
	LengthPropertyName    uint32
	PropertyName          string
	DataTypePropertyValue uint32
	LengthPropertyValue   uint32
	PropertyValue         string
}

func NewMetaData(file []byte, position int) (*MetaData, error) {
	if position < 1 {
		// TODO explain more in error
		return nil, fmt.Errorf("very low position")
	}
	metaData := MetaData{Position: position, Size: len(file)}

	var err error
	metaData.NewObjects, err = GetUint32(GetBytes(file, NumberOfObjects, &metaData.Position))
	if err != nil {
		return nil, err
	}

	metaData.LengthFirstObjectPath, err = GetUint32(GetBytes(file, LengthFirstObjectPath, &metaData.Position))
	if err != nil {
		return nil, err
	}

	metaData.ObjectPath = string(GetBytes(file, int(metaData.LengthFirstObjectPath), &metaData.Position))
	metaData.RawDataIndex = GetBytes(file, RawDataIndex, &metaData.Position)

	metaData.NumberOfProperties, err = GetUint32(GetBytes(file, NumberOfProperties, &metaData.Position))
	if err != nil {
		return nil, err
	}

	for i := 0; i < int(metaData.NumberOfProperties); i++ {
		prop, err := PropertyProcessing(file, &metaData.Position)
		if err != nil {
			return nil, err
		}
		metaData.Properties = append(metaData.Properties, prop)
	}

	// second property
	prop2, err := PropertyProcessing(file, &metaData.Position)
	if err != nil {
		return nil, err
	}

	metaData.Properties = append(metaData.Properties, prop2)

	for i := 0; i < 14; i++ {
		chanel, err := NewChannel(file, &metaData.Position)
		if err != nil {
			return nil, err
		}
		metaData.Channels = append(metaData.Channels, chanel)
	}

	//var raw []float64
	var ci, cn int

	// Fill out all row data
	for i := metaData.Position; i < metaData.Size; i += 8 {
		res, err := GetFloat64(GetBytes(file, 8, &metaData.Position))
		if err != nil {
			return nil, err
		}

		resCheck := fmt.Sprintf("%f", res)
		if len(resCheck) > 20 || res == 0 {
			fmt.Println(resCheck)
			continue
		}

		resCheck2, err := strconv.ParseFloat(resCheck, 64)
		if err != nil || resCheck2 == 0 {
			continue
		}

		metaData.Channels[cn].RowData = append(metaData.Channels[cn].RowData, res)
		ci++
		if ci == 15 {
			ci = 0
			cn++
			if cn == 14 {
				cn = 0
			}
		}
	}

	return &metaData, nil
}

func PropertyProcessing(file []byte, position *int) (Property, error) {
	prop := Property{}

	var err error
	prop.LengthPropertyName, err = GetUint32(GetBytes(file, LengthFirstPropertyName, position))
	if err != nil {
		return Property{}, err
	}

	prop.PropertyName = string(GetBytes(file, int(prop.LengthPropertyName), position))

	if prop.LengthPropertyName == 11 {
		li := GetBytes(file, DataTypePropertyValue, position)
		fmt.Println(li)
	}
	prop.DataTypePropertyValue, err = GetUint32(GetBytes(file, DataTypePropertyValue, position))
	if err != nil {
		return Property{}, err
	}

	if prop.DataTypePropertyValue == 32 {
		prop.LengthPropertyValue, err = GetUint32(GetBytes(file, LengthPropertyValue, position))
		if err != nil {
			return Property{}, err
		}
	}

	if prop.LengthPropertyValue != 0 {
		size := int(prop.LengthPropertyValue)
		prop.PropertyValue = string(GetBytes(file, size, position))
	} else if prop.DataTypePropertyValue == 68 {

		panic("TODO")

	}

	return prop, err
}
