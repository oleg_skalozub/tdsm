package segment

import (
	"encoding/hex"
)

//@see http://www.ni.com/product-documentation/5696/en/

const (
	//"TDSm" tag
	TDSmTag = 4
	//ToC mask 0x1110 (segment contains object list, meta data, raw data)
	ToCMask = 4
	//Version number (4713)
	VersionNumber = 4
	//Next segment offset
	SegmentOffset = 8
	//Raw data offset
	RawDataOffset = 8
)

type Data struct {
	Position          int
	DSmTag            string
	ToCMask           []byte
	Version           uint32
	NextSegmentOffset uint64
	RawDataOffset     uint64
}

func NewLead(file []byte) (*Data, error) {
	lead := Data{Position: 0}

	lead.DSmTag = hex.Dump(GetBytes(file, TDSmTag, &lead.Position))
	lead.ToCMask = GetBytes(file, ToCMask, &lead.Position)

	// todo add err description
	var err error
	lead.Version, err = GetUint32(GetBytes(file, VersionNumber, &lead.Position))
	if err != nil {
		return nil, err
	}

	// todo add err description
	lead.NextSegmentOffset, err = GetUint64(GetBytes(file, SegmentOffset, &lead.Position))
	if err != nil {
		return nil, err
	}

	// todo add err description
	lead.RawDataOffset, err = GetUint64(GetBytes(file, RawDataOffset, &lead.Position))
	if err != nil {
		return nil, err
	}

	return &lead, nil
}
