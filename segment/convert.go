package segment

import (
	"io/ioutil"
	"log"
)

func Convert(name string) *MetaData {
	res, err := ioutil.ReadFile(name)
	if err != nil {
		log.Fatal(err)
	}

	test, err := NewLead(res)
	metatest, err := NewMetaData(res, test.Position)

	return metatest
}
